# README #

ExpressCheckout client demo with Cocoapods EC SDK

### Install ###

* `pod install` or `pod update` 

### Setup ###

* Change the merchant Id and client key in ViewController.swift
* Pass the order Id in starting screen of the app

### Create Order ###

```
curl -X POST https://sandbox.juspay.in/orders -u api_key: -d "order_id=dummyorder1" -d "amount=10.00" -d "currency=INR" -d "customer_id=guest_user_101" -d "customer_email=customer@gmail.com" -d "customer_phone=9988665522" -d "product_id=prod-141833" -d "return_url=https://www.test.com/"
```

