//
//  StartViewController.swift
//  ECUrlBased_SwiftDemo
//
//  Created by Balaganesh S on 18/07/17.
//  Copyright © 2017 Juspay Technologies Pvt Ltd. All rights reserved.
//

import UIKit

class StartViewController: UIViewController {
    
    @IBOutlet var orderField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.edgesForExtendedLayout = [];
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func startButtonTapped(_ sender: Any) {
        
        let payment:ViewController = ViewController()
        payment.orderId = orderField.text
        self.navigationController?.pushViewController(payment, animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
