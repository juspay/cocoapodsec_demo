//
//  ViewController.swift
//  ECUrlBased_SwiftDemo
//
//  Created by Sachin Sharma on 06/07/16.
//  Copyright © 2016 Juspay Technologies Pvt Ltd. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var orderId: String?
    let merchantId = "YOUR_MERCHANT_ID"
    
    var endUrlRegex = ["https:\\/\\/www.test.com\\/.*"]
    
    let expressCheckout = ExpressCheckout(clientKey: "CLIENT_KEY", environment: PRODUCTION)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.edgesForExtendedLayout = []
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false;
        
//        expressCheckout?.shouldLoadEndURL = true
//        expressCheckout?.shouldNotPopOnEndURL = true
//        expressCheckout?.shouldNotPopAfterPayment = true
        
        expressCheckout?.confirmationAlertContents = ["Cancel current transaction?", "It would terminate the ongoing payment process.", "OK", "Cancel"]
        
        expressCheckout?.environment(SANDBOX, merchantId: merchantId, orderId: orderId, endUrlRegexes: endUrlRegex)
        
        expressCheckout?.startPayment(in: self.view) { status, error, info in
            print(info!);
        };
        
    }
    
    override func navigationShouldPopOnBackButton() -> Bool {
        expressCheckout?.backButtonPressed()
        return expressCheckout!.isControllerAllowedToPop()
    }

    override func viewDidLayoutSubviews() {

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

